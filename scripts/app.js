//-----------------------
// -- HELPER functions --

Array.prototype.loop = function(theFunction) {
	for (var i = 0, i_l = this.length; i < i_l; i++) {
		if (theFunction(this[i], i) == false) {
			break;
		}
	}
};

////--------------------------------------------------


//-----------------
// -- Github API --

const github = new Vue({
	data: {
		api_url: 'https://api.github.com',
		username: 'huszerldani',
		client_id: 'cf3b7e47964edf10a8088a8217a66d9b2884fb8f'
	},
	computed: {
		headers: function() {
			return {
				"Authorization": "Basic " + btoa(this.username + ":" + this.client_id)
			};
		}
	},
	methods: {
		get: function(url, success = null) {
			$.ajax({
				type: "GET",
				url: this.api_url + url,
				headers: this.headers,
				success: function (response) {
					if (success) {
						success(response);
					}
				}
			});
		},
		post: function(url, data = {}, success = null) {
			$.ajax({
				type: "POST",
				data: JSON.stringify(data),
				dataType: "json",
				url: this.api_url + url,
				headers: this.headers,
				success: function (response) {
					if (success) {
						success(response);
					}
				}
			});
		},
		patch: function(url, data, success = null) {
			$.ajax({
				type: "PATCH",
				data: JSON.stringify(data),
				dataType: "json",
				url: this.api_url + url,
				headers: this.headers,
				success: function (response) {
					if (success) {
						success(response);
					}
				}
			});
		}
	}
});


//-----------
// -- VUEX --

const store = new Vuex.Store({
	state: {
		user: null,
		gist: null
	},
	mutations: {
		removeFile: function(state, data, success = null) {
			var data_to_send = {
				description: state.gist.description,
				files: data
			}
			
			github.patch('/gists/'+state.gist.id, data_to_send, function(response) {
				state.gist = response;
				if (success) {
					success(response);
				}
			});
		},
		updateFile: function(state, data, success = null) {
			var dat = {};

			dat[data.filename] = {
				filename: data.filename,
				content: data.content.length == 0 ? '-' : data.content
			}

			var data_to_send = {
				description: state.gist.description,
				files: dat
			}

			github.patch('/gists/'+state.gist.id, data_to_send, function(response) {
				state.gist = response;
				if (success) {
					success(response);
				}
			});
		},
		updateGist: function(state, content) {
			//this.$store.commit('updateGist', this.content)
		},
		getUser: function(state) {
			github.get('/user', function(response) {
				state.user = response;
			});
		},
		getGist: function(state) {
			var that = this;

			github.get('/gists', function(gists) {
				var gist = null;

				gists.loop(function(g) {
					if (g.description != 'Created by Github Notes') {
						return;
					}

					gist = g;
					return false;
				});
			
				if (gist == null) {
					that.commit('createInitGist');
					
					return;
				}

				github.get('/gists/'+gist.id, function(response) {
					state.gist = response;
				});
			});
		},
		createInitGist: function(state) {
			console.info("Gist not found. Let's create it.");

			var data = {
				description: "Created by Github Notes",
				public: true,
				files: {
					'welcome.txt': {
						content: '- Welcome on board -'
					}
				}
			};
			
			github.post('/gists', data, function(response) {
				state.gist = response;
			});
		}
	}
});


//---------------------
// -- List component --

const List = {
	name: 'List',
	template: '#template--list',
	mounted: function() {
		if (this.$route.params.filename) {
			this.$root.openFile(this.$route.params.filename);
		}
	},
	watch: {
		'$route.params.filename': function(filename) {
			this.$root.openFile(filename);
		}
	},
	methods: {
		removeFile: function(file) {
			if (!confirm('Remove file?')) {
				return;
			}
			
			var data = {};

			data[file.filename] = {
				filename: file.filename,
				content: ''
			}

			this.$store.commit('removeFile', data);
			this.$root.selected = null;
			this.$root.opened = null;
		}
	}
}


//---------------------
// -- Edit component --

const Edit = {
	name: 'Edit',
	template: '#template--edit',
	data: function() {
		return {
			file: null
		}
	},
	watch: {
		'$route.params.filename': function(filename) {
			this.$root.editing = false;
			this.$root.opened = filename;

			this.getFile();
		}
	},
	mounted: function() {
		this.getFile();

		if (this.$route.params.filename) {
			this.$root.opened = this.$route.params.filename;
		}
	},
	methods: {
		saveFile: function() {
			this.$store.commit('updateFile', this.file);
		},
		getFile: function() {
			if (this.$store.state.gist == null) {
				var that = this;

				setTimeout(function() {
					that.getFile();
				}, 300);

				return;
			}


			//console.log(this.$store.state.gist.files);
			
			var file = null;
			for(f in this.$store.state.gist.files) {
				if (this.$store.state.gist.files[f].filename == this.$route.params.filename) {
					file = this.$store.state.gist.files[f];
				}
			}

			if (file == null) {
				//console.error("File not found");
				alert("File not found");
				return;
			}

			this.file = file;
		}
	}
}


//---------------------
// -- Create component --

const Create = {
	name: 'Create',
	template: '#template--create',
	data: function() {
		return {
			form: {
				filename: '',
				content: ''
			}
		}
	},
	methods: {
		saveFile: function() {
			this.$store.commit('updateFile', this.form);
			this.form = {
				filename: '',
				content: ''
			};
		}
	}
}


//---------------------
// -- Empty component --

const Empty = {
	name: 'Empty',
	template: '#template--empty'
}


//-----------------
// -- VUE router --

const routes = [
	{
		path: '/files/create',
		component: Create
	},{
		path: '/files/:filename',
		component: List,
	}, {
		path: '/files/:filename/edit',
		component: Edit
	}, {
		path: '*',
		component: Empty
	}
];

const router = new VueRouter({
	routes: routes,
	mode: 'history'
});


//--------------
// -- VUE app --

const vue = new Vue({
	el: '#vue',
	router: router,
	store: store,
	data: {
		opened: null,
		selected: null,
		editing: false
	},
	watch: {
		'opened': function() {
			
		}
	},
	mounted: function() {
		this.$store.commit('getUser');
		this.$store.commit('getGist');
	},
	methods: {
		openFile: function(filename) {
			var that = this;
			
			if (this.$store.state.gist == null) {
				setTimeout(function() {
					that.openFile(filename);
				}, 200);
				return;	
			}

			this.opened = filename;

			var file = null;
			for(f in this.$store.state.gist.files) {
				if (this.$store.state.gist.files[f].filename == filename) {
					file = this.$store.state.gist.files[f];
				}
			}

			if (file == null) {
				//console.error("File not found");
				alert("File not found");
				return;
			}

			if (this.$route.path != '/') {
				//this.$router.push('/');
			}

			this.selected = file;
		}
	}
});