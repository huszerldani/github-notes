// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"scripts/app.js":[function(require,module,exports) {
//-----------------------
// -- HELPER functions --
Array.prototype.loop = function (theFunction) {
  for (var i = 0, i_l = this.length; i < i_l; i++) {
    if (theFunction(this[i], i) == false) {
      break;
    }
  }
}; ////--------------------------------------------------
//-----------------
// -- Github API --


var github = new Vue({
  data: {
    api_url: 'https://api.github.com',
    username: 'huszerldani',
    client_id: 'cf3b7e47964edf10a8088a8217a66d9b2884fb8f'
  },
  computed: {
    headers: function headers() {
      return {
        "Authorization": "Basic " + btoa(this.username + ":" + this.client_id)
      };
    }
  },
  methods: {
    get: function get(url) {
      var _success = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      $.ajax({
        type: "GET",
        url: this.api_url + url,
        headers: this.headers,
        success: function success(response) {
          if (_success) {
            _success(response);
          }
        }
      });
    },
    post: function post(url) {
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var _success2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      $.ajax({
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        url: this.api_url + url,
        headers: this.headers,
        success: function success(response) {
          if (_success2) {
            _success2(response);
          }
        }
      });
    },
    patch: function patch(url, data) {
      var _success3 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      $.ajax({
        type: "PATCH",
        data: JSON.stringify(data),
        dataType: "json",
        url: this.api_url + url,
        headers: this.headers,
        success: function success(response) {
          if (_success3) {
            _success3(response);
          }
        }
      });
    }
  }
}); //-----------
// -- VUEX --

var store = new Vuex.Store({
  state: {
    user: null,
    gist: null
  },
  mutations: {
    removeFile: function removeFile(state, data) {
      var success = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var data_to_send = {
        description: state.gist.description,
        files: data
      };
      github.patch('/gists/' + state.gist.id, data_to_send, function (response) {
        state.gist = response;

        if (success) {
          success(response);
        }
      });
    },
    updateFile: function updateFile(state, data) {
      var success = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var dat = {};
      dat[data.filename] = {
        filename: data.filename,
        content: data.content.length == 0 ? '-' : data.content
      };
      var data_to_send = {
        description: state.gist.description,
        files: dat
      };
      github.patch('/gists/' + state.gist.id, data_to_send, function (response) {
        state.gist = response;

        if (success) {
          success(response);
        }
      });
    },
    updateGist: function updateGist(state, content) {//this.$store.commit('updateGist', this.content)
    },
    getUser: function getUser(state) {
      github.get('/user', function (response) {
        state.user = response;
      });
    },
    getGist: function getGist(state) {
      var that = this;
      github.get('/gists', function (gists) {
        var gist = null;
        gists.loop(function (g) {
          if (g.description != 'Created by Github Notes') {
            return;
          }

          gist = g;
          return false;
        });

        if (gist == null) {
          that.commit('createInitGist');
          return;
        }

        github.get('/gists/' + gist.id, function (response) {
          state.gist = response;
        });
      });
    },
    createInitGist: function createInitGist(state) {
      console.info("Gist not found. Let's create it.");
      var data = {
        description: "Created by Github Notes",
        public: true,
        files: {
          'welcome.txt': {
            content: '- Welcome on board -'
          }
        }
      };
      github.post('/gists', data, function (response) {
        state.gist = response;
      });
    }
  }
}); //---------------------
// -- List component --

var List = {
  name: 'List',
  template: '#template--list',
  mounted: function mounted() {
    if (this.$route.params.filename) {
      this.$root.openFile(this.$route.params.filename);
    }
  },
  watch: {
    '$route.params.filename': function $routeParamsFilename(filename) {
      this.$root.openFile(filename);
    }
  },
  methods: {
    removeFile: function removeFile(file) {
      if (!confirm('Remove file?')) {
        return;
      }

      var data = {};
      data[file.filename] = {
        filename: file.filename,
        content: ''
      };
      this.$store.commit('removeFile', data);
      this.$root.selected = null;
      this.$root.opened = null;
    }
  } //---------------------
  // -- Edit component --

};
var Edit = {
  name: 'Edit',
  template: '#template--edit',
  data: function data() {
    return {
      file: null
    };
  },
  watch: {
    '$route.params.filename': function $routeParamsFilename(filename) {
      this.$root.editing = false;
      this.$root.opened = filename;
      this.getFile();
    }
  },
  mounted: function mounted() {
    this.getFile();

    if (this.$route.params.filename) {
      this.$root.opened = this.$route.params.filename;
    }
  },
  methods: {
    saveFile: function saveFile() {
      this.$store.commit('updateFile', this.file);
    },
    getFile: function getFile() {
      if (this.$store.state.gist == null) {
        var that = this;
        setTimeout(function () {
          that.getFile();
        }, 300);
        return;
      } //console.log(this.$store.state.gist.files);


      var file = null;

      for (f in this.$store.state.gist.files) {
        if (this.$store.state.gist.files[f].filename == this.$route.params.filename) {
          file = this.$store.state.gist.files[f];
        }
      }

      if (file == null) {
        //console.error("File not found");
        alert("File not found");
        return;
      }

      this.file = file;
    }
  } //---------------------
  // -- Create component --

};
var Create = {
  name: 'Create',
  template: '#template--create',
  data: function data() {
    return {
      form: {
        filename: '',
        content: ''
      }
    };
  },
  methods: {
    saveFile: function saveFile() {
      this.$store.commit('updateFile', this.form);
      this.form = {
        filename: '',
        content: ''
      };
    }
  } //---------------------
  // -- Empty component --

};
var Empty = {
  name: 'Empty',
  template: '#template--empty' //-----------------
  // -- VUE router --

};
var routes = [{
  path: '/files/create',
  component: Create
}, {
  path: '/files/:filename',
  component: List
}, {
  path: '/files/:filename/edit',
  component: Edit
}, {
  path: '*',
  component: Empty
}];
var router = new VueRouter({
  routes: routes,
  mode: 'history'
}); //--------------
// -- VUE app --

var vue = new Vue({
  el: '#vue',
  router: router,
  store: store,
  data: {
    opened: null,
    selected: null,
    editing: false
  },
  watch: {
    'opened': function opened() {}
  },
  mounted: function mounted() {
    this.$store.commit('getUser');
    this.$store.commit('getGist');
  },
  methods: {
    openFile: function openFile(filename) {
      var that = this;

      if (this.$store.state.gist == null) {
        setTimeout(function () {
          that.openFile(filename);
        }, 200);
        return;
      }

      this.opened = filename;
      var file = null;

      for (f in this.$store.state.gist.files) {
        if (this.$store.state.gist.files[f].filename == filename) {
          file = this.$store.state.gist.files[f];
        }
      }

      if (file == null) {
        //console.error("File not found");
        alert("File not found");
        return;
      }

      if (this.$route.path != '/') {//this.$router.push('/');
      }

      this.selected = file;
    }
  }
});
},{}],"../../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "43358" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","scripts/app.js"], null)
//# sourceMappingURL=/app.c09d0a7b.js.map